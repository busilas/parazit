# PARAZIT Restaurant Web App

This is a final project app for the 3WA.

This app was made using Laravel. 

## Local Instalation 
Clone the repo and cd into the directory, then run composer install form the command line. Setting up the SQLite Database (database file: db.sqlite) on .env. After installation has finished run: 
	
	cd parazit
	
	composer install
	
	php artisan migrate

	php artisan db:seed

	php artisan serve 

from the command line. The App uses sqlite in development enviroment. You should browse the app at:
	
	localhost:8000

To test application the database is seeding with users :

	Administrator : email = admin@parazit.com, password = admin
	User : email = user@parazit.com, password = user123

## Features

	Restaurant Index page
	Menu page (displays all dishes. There is an option to filter dishes by category)
	Reservation page (gust filling form can make a reservation).
	Contact page (restaurant info, location with googlemaps).
	Cart page (dispalys dishes during the ordering. Visitor can increase/decrease quantity of dishes).
	Authentication (registration, login, logout, password reset)
	Checkout page (user has to be registered or loged in).
	Profile page (user has to be registered or loged in. User can update profile info).
	Orders page (user has to be registered or loged in. User can see his/her purchase history).
	

	Admin index page
	Product admin page (show, edit, delete, create)
	Categories admin page (show, edit, delete, create). Categories plays as a filter features for Menu page.
	Orders admin page (view all details of orders)
	Reservations admin page (show, edit, delete)
	Users admin page (show, edit, delete, create)

	Admin after login redirects to admin/index. Admin can access admin dashboard from restaurant page by menu button ADMIN PANEL.

##Users roles : 	

	admin (all access), 
	user (checkout, upload personal profile, view orders history), 	
	and guest (visit: Restaurant Index / Menu (can order) / Reservation (can make resrvation) / Contact / Cart pages (view/manage ordered dishes quantity)).